<?php
/**
 * ModelLaudoParceiros
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisLol
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * LaudosONLine API
 *
 * Laudos On Line API
 *
 * OpenAPI spec version: V1
 * Contact: ronaldo@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisLol\Model;

use \ArrayAccess;
use \VertisLol\ObjectSerializer;

/**
 * ModelLaudoParceiros Class Doc Comment
 *
 * @category Class
 * @package  VertisLol
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelLaudoParceiros implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelLaudoParceiros';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id_nl_ldo_disp_parceiro' => 'int',
        'fkid_nl_laudos_disp' => 'int',
        'fkid_parceiro' => 'int',
        'ind_laudo_lido' => 'string',
        'cod_unid_negoc' => 'int',
        'cod_unid_oper' => 'int',
        'cod_ord_servico' => 'int',
        'cod_parceiro' => 'int',
        'contingencia' => 'bool',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id_nl_ldo_disp_parceiro' => null,
        'fkid_nl_laudos_disp' => null,
        'fkid_parceiro' => null,
        'ind_laudo_lido' => null,
        'cod_unid_negoc' => null,
        'cod_unid_oper' => null,
        'cod_ord_servico' => null,
        'cod_parceiro' => null,
        'contingencia' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id_nl_ldo_disp_parceiro' => 'id_nl_ldo_disp_parceiro',
        'fkid_nl_laudos_disp' => 'fkid_nl_laudos_disp',
        'fkid_parceiro' => 'fkid_parceiro',
        'ind_laudo_lido' => 'ind_laudo_lido',
        'cod_unid_negoc' => 'cod_unid_negoc',
        'cod_unid_oper' => 'cod_unid_oper',
        'cod_ord_servico' => 'cod_ord_servico',
        'cod_parceiro' => 'cod_parceiro',
        'contingencia' => 'contingencia',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id_nl_ldo_disp_parceiro' => 'setIdNlLdoDispParceiro',
        'fkid_nl_laudos_disp' => 'setFkidNlLaudosDisp',
        'fkid_parceiro' => 'setFkidParceiro',
        'ind_laudo_lido' => 'setIndLaudoLido',
        'cod_unid_negoc' => 'setCodUnidNegoc',
        'cod_unid_oper' => 'setCodUnidOper',
        'cod_ord_servico' => 'setCodOrdServico',
        'cod_parceiro' => 'setCodParceiro',
        'contingencia' => 'setContingencia',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id_nl_ldo_disp_parceiro' => 'getIdNlLdoDispParceiro',
        'fkid_nl_laudos_disp' => 'getFkidNlLaudosDisp',
        'fkid_parceiro' => 'getFkidParceiro',
        'ind_laudo_lido' => 'getIndLaudoLido',
        'cod_unid_negoc' => 'getCodUnidNegoc',
        'cod_unid_oper' => 'getCodUnidOper',
        'cod_ord_servico' => 'getCodOrdServico',
        'cod_parceiro' => 'getCodParceiro',
        'contingencia' => 'getContingencia',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id_nl_ldo_disp_parceiro'] = isset($data['id_nl_ldo_disp_parceiro']) ? $data['id_nl_ldo_disp_parceiro'] : null;
        $this->container['fkid_nl_laudos_disp'] = isset($data['fkid_nl_laudos_disp']) ? $data['fkid_nl_laudos_disp'] : null;
        $this->container['fkid_parceiro'] = isset($data['fkid_parceiro']) ? $data['fkid_parceiro'] : null;
        $this->container['ind_laudo_lido'] = isset($data['ind_laudo_lido']) ? $data['ind_laudo_lido'] : null;
        $this->container['cod_unid_negoc'] = isset($data['cod_unid_negoc']) ? $data['cod_unid_negoc'] : null;
        $this->container['cod_unid_oper'] = isset($data['cod_unid_oper']) ? $data['cod_unid_oper'] : null;
        $this->container['cod_ord_servico'] = isset($data['cod_ord_servico']) ? $data['cod_ord_servico'] : null;
        $this->container['cod_parceiro'] = isset($data['cod_parceiro']) ? $data['cod_parceiro'] : null;
        $this->container['contingencia'] = isset($data['contingencia']) ? $data['contingencia'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id_nl_ldo_disp_parceiro'] === null) {
            $invalidProperties[] = "'id_nl_ldo_disp_parceiro' can't be null";
        }
        if ($this->container['fkid_nl_laudos_disp'] === null) {
            $invalidProperties[] = "'fkid_nl_laudos_disp' can't be null";
        }
        if ($this->container['fkid_parceiro'] === null) {
            $invalidProperties[] = "'fkid_parceiro' can't be null";
        }
        if ($this->container['ind_laudo_lido'] === null) {
            $invalidProperties[] = "'ind_laudo_lido' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id_nl_ldo_disp_parceiro
     *
     * @return int
     */
    public function getIdNlLdoDispParceiro()
    {
        return $this->container['id_nl_ldo_disp_parceiro'];
    }

    /**
     * Sets id_nl_ldo_disp_parceiro
     *
     * @param int $id_nl_ldo_disp_parceiro #field_definition#
     *
     * @return $this
     */
    public function setIdNlLdoDispParceiro($id_nl_ldo_disp_parceiro)
    {
        $this->container['id_nl_ldo_disp_parceiro'] = $id_nl_ldo_disp_parceiro;

        return $this;
    }

    /**
     * Gets fkid_nl_laudos_disp
     *
     * @return int
     */
    public function getFkidNlLaudosDisp()
    {
        return $this->container['fkid_nl_laudos_disp'];
    }

    /**
     * Sets fkid_nl_laudos_disp
     *
     * @param int $fkid_nl_laudos_disp #field_definition#
     *
     * @return $this
     */
    public function setFkidNlLaudosDisp($fkid_nl_laudos_disp)
    {
        $this->container['fkid_nl_laudos_disp'] = $fkid_nl_laudos_disp;

        return $this;
    }

    /**
     * Gets fkid_parceiro
     *
     * @return int
     */
    public function getFkidParceiro()
    {
        return $this->container['fkid_parceiro'];
    }

    /**
     * Sets fkid_parceiro
     *
     * @param int $fkid_parceiro #field_definition#
     *
     * @return $this
     */
    public function setFkidParceiro($fkid_parceiro)
    {
        $this->container['fkid_parceiro'] = $fkid_parceiro;

        return $this;
    }

    /**
     * Gets ind_laudo_lido
     *
     * @return string
     */
    public function getIndLaudoLido()
    {
        return $this->container['ind_laudo_lido'];
    }

    /**
     * Sets ind_laudo_lido
     *
     * @param string $ind_laudo_lido #field_definition#
     *
     * @return $this
     */
    public function setIndLaudoLido($ind_laudo_lido)
    {
        $this->container['ind_laudo_lido'] = $ind_laudo_lido;

        return $this;
    }

    /**
     * Gets cod_unid_negoc
     *
     * @return int
     */
    public function getCodUnidNegoc()
    {
        return $this->container['cod_unid_negoc'];
    }

    /**
     * Sets cod_unid_negoc
     *
     * @param int $cod_unid_negoc cod_unid_negoc
     *
     * @return $this
     */
    public function setCodUnidNegoc($cod_unid_negoc)
    {
        $this->container['cod_unid_negoc'] = $cod_unid_negoc;

        return $this;
    }

    /**
     * Gets cod_unid_oper
     *
     * @return int
     */
    public function getCodUnidOper()
    {
        return $this->container['cod_unid_oper'];
    }

    /**
     * Sets cod_unid_oper
     *
     * @param int $cod_unid_oper cod_unid_oper
     *
     * @return $this
     */
    public function setCodUnidOper($cod_unid_oper)
    {
        $this->container['cod_unid_oper'] = $cod_unid_oper;

        return $this;
    }

    /**
     * Gets cod_ord_servico
     *
     * @return int
     */
    public function getCodOrdServico()
    {
        return $this->container['cod_ord_servico'];
    }

    /**
     * Sets cod_ord_servico
     *
     * @param int $cod_ord_servico cod_ord_servico
     *
     * @return $this
     */
    public function setCodOrdServico($cod_ord_servico)
    {
        $this->container['cod_ord_servico'] = $cod_ord_servico;

        return $this;
    }

    /**
     * Gets cod_parceiro
     *
     * @return int
     */
    public function getCodParceiro()
    {
        return $this->container['cod_parceiro'];
    }

    /**
     * Sets cod_parceiro
     *
     * @param int $cod_parceiro cod_parceiro
     *
     * @return $this
     */
    public function setCodParceiro($cod_parceiro)
    {
        $this->container['cod_parceiro'] = $cod_parceiro;

        return $this;
    }

    /**
     * Gets contingencia
     *
     * @return bool
     */
    public function getContingencia()
    {
        return $this->container['contingencia'];
    }

    /**
     * Sets contingencia
     *
     * @param bool $contingencia contingencia
     *
     * @return $this
     */
    public function setContingencia($contingencia)
    {
        $this->container['contingencia'] = $contingencia;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


