<?php
/**
 * ServiceMail
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisLol
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * LaudosONLine API
 *
 * Laudos On Line API
 *
 * OpenAPI spec version: V1
 * Contact: ronaldo@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisLol\Model;

use \ArrayAccess;
use \VertisLol\ObjectSerializer;

/**
 * ServiceMail Class Doc Comment
 *
 * @category Class
 * @package  VertisLol
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ServiceMail implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ServiceMail';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'host' => 'string',
        'port' => 'int',
        'user_name' => 'string',
        'password' => 'string',
        'connect_timeout' => 'int',
        'ssl_connection' => 'bool',
        'tls_mode' => 'string',
        'nome_remetente' => 'string',
        'e_mail_remetente' => 'string',
        'destinatarios' => 'string',
        'assunto' => 'string',
        'message_body' => 'string',
        'unid_oper' => 'int',
        'unid_neg' => 'int',
        'modo' => 'string',
        'path_log' => 'string',
        'nome_log' => 'string',
        'anexos' => '\VertisLol\Model\ServiceMailAnexos[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'host' => null,
        'port' => null,
        'user_name' => null,
        'password' => null,
        'connect_timeout' => null,
        'ssl_connection' => null,
        'tls_mode' => null,
        'nome_remetente' => null,
        'e_mail_remetente' => null,
        'destinatarios' => null,
        'assunto' => null,
        'message_body' => null,
        'unid_oper' => null,
        'unid_neg' => null,
        'modo' => null,
        'path_log' => null,
        'nome_log' => null,
        'anexos' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'host' => 'Host',
        'port' => 'Port',
        'user_name' => 'UserName',
        'password' => 'Password',
        'connect_timeout' => 'ConnectTimeout',
        'ssl_connection' => 'SSLConnection',
        'tls_mode' => 'TLSMode',
        'nome_remetente' => 'NomeRemetente',
        'e_mail_remetente' => 'EMailRemetente',
        'destinatarios' => 'Destinatarios',
        'assunto' => 'Assunto',
        'message_body' => 'MessageBody',
        'unid_oper' => 'UnidOper',
        'unid_neg' => 'UnidNeg',
        'modo' => 'Modo',
        'path_log' => 'PathLog',
        'nome_log' => 'NomeLog',
        'anexos' => 'anexos'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'host' => 'setHost',
        'port' => 'setPort',
        'user_name' => 'setUserName',
        'password' => 'setPassword',
        'connect_timeout' => 'setConnectTimeout',
        'ssl_connection' => 'setSslConnection',
        'tls_mode' => 'setTlsMode',
        'nome_remetente' => 'setNomeRemetente',
        'e_mail_remetente' => 'setEMailRemetente',
        'destinatarios' => 'setDestinatarios',
        'assunto' => 'setAssunto',
        'message_body' => 'setMessageBody',
        'unid_oper' => 'setUnidOper',
        'unid_neg' => 'setUnidNeg',
        'modo' => 'setModo',
        'path_log' => 'setPathLog',
        'nome_log' => 'setNomeLog',
        'anexos' => 'setAnexos'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'host' => 'getHost',
        'port' => 'getPort',
        'user_name' => 'getUserName',
        'password' => 'getPassword',
        'connect_timeout' => 'getConnectTimeout',
        'ssl_connection' => 'getSslConnection',
        'tls_mode' => 'getTlsMode',
        'nome_remetente' => 'getNomeRemetente',
        'e_mail_remetente' => 'getEMailRemetente',
        'destinatarios' => 'getDestinatarios',
        'assunto' => 'getAssunto',
        'message_body' => 'getMessageBody',
        'unid_oper' => 'getUnidOper',
        'unid_neg' => 'getUnidNeg',
        'modo' => 'getModo',
        'path_log' => 'getPathLog',
        'nome_log' => 'getNomeLog',
        'anexos' => 'getAnexos'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['host'] = isset($data['host']) ? $data['host'] : null;
        $this->container['port'] = isset($data['port']) ? $data['port'] : null;
        $this->container['user_name'] = isset($data['user_name']) ? $data['user_name'] : null;
        $this->container['password'] = isset($data['password']) ? $data['password'] : null;
        $this->container['connect_timeout'] = isset($data['connect_timeout']) ? $data['connect_timeout'] : null;
        $this->container['ssl_connection'] = isset($data['ssl_connection']) ? $data['ssl_connection'] : null;
        $this->container['tls_mode'] = isset($data['tls_mode']) ? $data['tls_mode'] : null;
        $this->container['nome_remetente'] = isset($data['nome_remetente']) ? $data['nome_remetente'] : null;
        $this->container['e_mail_remetente'] = isset($data['e_mail_remetente']) ? $data['e_mail_remetente'] : null;
        $this->container['destinatarios'] = isset($data['destinatarios']) ? $data['destinatarios'] : null;
        $this->container['assunto'] = isset($data['assunto']) ? $data['assunto'] : null;
        $this->container['message_body'] = isset($data['message_body']) ? $data['message_body'] : null;
        $this->container['unid_oper'] = isset($data['unid_oper']) ? $data['unid_oper'] : null;
        $this->container['unid_neg'] = isset($data['unid_neg']) ? $data['unid_neg'] : null;
        $this->container['modo'] = isset($data['modo']) ? $data['modo'] : null;
        $this->container['path_log'] = isset($data['path_log']) ? $data['path_log'] : null;
        $this->container['nome_log'] = isset($data['nome_log']) ? $data['nome_log'] : null;
        $this->container['anexos'] = isset($data['anexos']) ? $data['anexos'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->container['host'];
    }

    /**
     * Sets host
     *
     * @param string $host host
     *
     * @return $this
     */
    public function setHost($host)
    {
        $this->container['host'] = $host;

        return $this;
    }

    /**
     * Gets port
     *
     * @return int
     */
    public function getPort()
    {
        return $this->container['port'];
    }

    /**
     * Sets port
     *
     * @param int $port port
     *
     * @return $this
     */
    public function setPort($port)
    {
        $this->container['port'] = $port;

        return $this;
    }

    /**
     * Gets user_name
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->container['user_name'];
    }

    /**
     * Sets user_name
     *
     * @param string $user_name user_name
     *
     * @return $this
     */
    public function setUserName($user_name)
    {
        $this->container['user_name'] = $user_name;

        return $this;
    }

    /**
     * Gets password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->container['password'];
    }

    /**
     * Sets password
     *
     * @param string $password password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->container['password'] = $password;

        return $this;
    }

    /**
     * Gets connect_timeout
     *
     * @return int
     */
    public function getConnectTimeout()
    {
        return $this->container['connect_timeout'];
    }

    /**
     * Sets connect_timeout
     *
     * @param int $connect_timeout connect_timeout
     *
     * @return $this
     */
    public function setConnectTimeout($connect_timeout)
    {
        $this->container['connect_timeout'] = $connect_timeout;

        return $this;
    }

    /**
     * Gets ssl_connection
     *
     * @return bool
     */
    public function getSslConnection()
    {
        return $this->container['ssl_connection'];
    }

    /**
     * Sets ssl_connection
     *
     * @param bool $ssl_connection ssl_connection
     *
     * @return $this
     */
    public function setSslConnection($ssl_connection)
    {
        $this->container['ssl_connection'] = $ssl_connection;

        return $this;
    }

    /**
     * Gets tls_mode
     *
     * @return string
     */
    public function getTlsMode()
    {
        return $this->container['tls_mode'];
    }

    /**
     * Sets tls_mode
     *
     * @param string $tls_mode tls_mode
     *
     * @return $this
     */
    public function setTlsMode($tls_mode)
    {
        $this->container['tls_mode'] = $tls_mode;

        return $this;
    }

    /**
     * Gets nome_remetente
     *
     * @return string
     */
    public function getNomeRemetente()
    {
        return $this->container['nome_remetente'];
    }

    /**
     * Sets nome_remetente
     *
     * @param string $nome_remetente nome_remetente
     *
     * @return $this
     */
    public function setNomeRemetente($nome_remetente)
    {
        $this->container['nome_remetente'] = $nome_remetente;

        return $this;
    }

    /**
     * Gets e_mail_remetente
     *
     * @return string
     */
    public function getEMailRemetente()
    {
        return $this->container['e_mail_remetente'];
    }

    /**
     * Sets e_mail_remetente
     *
     * @param string $e_mail_remetente e_mail_remetente
     *
     * @return $this
     */
    public function setEMailRemetente($e_mail_remetente)
    {
        $this->container['e_mail_remetente'] = $e_mail_remetente;

        return $this;
    }

    /**
     * Gets destinatarios
     *
     * @return string
     */
    public function getDestinatarios()
    {
        return $this->container['destinatarios'];
    }

    /**
     * Sets destinatarios
     *
     * @param string $destinatarios destinatarios
     *
     * @return $this
     */
    public function setDestinatarios($destinatarios)
    {
        $this->container['destinatarios'] = $destinatarios;

        return $this;
    }

    /**
     * Gets assunto
     *
     * @return string
     */
    public function getAssunto()
    {
        return $this->container['assunto'];
    }

    /**
     * Sets assunto
     *
     * @param string $assunto assunto
     *
     * @return $this
     */
    public function setAssunto($assunto)
    {
        $this->container['assunto'] = $assunto;

        return $this;
    }

    /**
     * Gets message_body
     *
     * @return string
     */
    public function getMessageBody()
    {
        return $this->container['message_body'];
    }

    /**
     * Sets message_body
     *
     * @param string $message_body message_body
     *
     * @return $this
     */
    public function setMessageBody($message_body)
    {
        $this->container['message_body'] = $message_body;

        return $this;
    }

    /**
     * Gets unid_oper
     *
     * @return int
     */
    public function getUnidOper()
    {
        return $this->container['unid_oper'];
    }

    /**
     * Sets unid_oper
     *
     * @param int $unid_oper unid_oper
     *
     * @return $this
     */
    public function setUnidOper($unid_oper)
    {
        $this->container['unid_oper'] = $unid_oper;

        return $this;
    }

    /**
     * Gets unid_neg
     *
     * @return int
     */
    public function getUnidNeg()
    {
        return $this->container['unid_neg'];
    }

    /**
     * Sets unid_neg
     *
     * @param int $unid_neg unid_neg
     *
     * @return $this
     */
    public function setUnidNeg($unid_neg)
    {
        $this->container['unid_neg'] = $unid_neg;

        return $this;
    }

    /**
     * Gets modo
     *
     * @return string
     */
    public function getModo()
    {
        return $this->container['modo'];
    }

    /**
     * Sets modo
     *
     * @param string $modo modo
     *
     * @return $this
     */
    public function setModo($modo)
    {
        $this->container['modo'] = $modo;

        return $this;
    }

    /**
     * Gets path_log
     *
     * @return string
     */
    public function getPathLog()
    {
        return $this->container['path_log'];
    }

    /**
     * Sets path_log
     *
     * @param string $path_log path_log
     *
     * @return $this
     */
    public function setPathLog($path_log)
    {
        $this->container['path_log'] = $path_log;

        return $this;
    }

    /**
     * Gets nome_log
     *
     * @return string
     */
    public function getNomeLog()
    {
        return $this->container['nome_log'];
    }

    /**
     * Sets nome_log
     *
     * @param string $nome_log nome_log
     *
     * @return $this
     */
    public function setNomeLog($nome_log)
    {
        $this->container['nome_log'] = $nome_log;

        return $this;
    }

    /**
     * Gets anexos
     *
     * @return \VertisLol\Model\ServiceMailAnexos[]
     */
    public function getAnexos()
    {
        return $this->container['anexos'];
    }

    /**
     * Sets anexos
     *
     * @param \VertisLol\Model\ServiceMailAnexos[] $anexos anexos
     *
     * @return $this
     */
    public function setAnexos($anexos)
    {
        $this->container['anexos'] = $anexos;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


