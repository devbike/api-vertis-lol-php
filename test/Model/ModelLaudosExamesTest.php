<?php
/**
 * ModelLaudosExamesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisLol
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * LaudosONLine API
 *
 * Laudos On Line API
 *
 * OpenAPI spec version: V1
 * Contact: ronaldo@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisLol;

/**
 * ModelLaudosExamesTest Class Doc Comment
 *
 * @category    Class
 * @description ModelLaudosExames
 * @package     VertisLol
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelLaudosExamesTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelLaudosExames"
     */
    public function testModelLaudosExames()
    {
    }

    /**
     * Test attribute "id_unid_oper"
     */
    public function testPropertyIdUnidOper()
    {
    }

    /**
     * Test attribute "cod_unid_negoc"
     */
    public function testPropertyCodUnidNegoc()
    {
    }

    /**
     * Test attribute "cod_unid_oper"
     */
    public function testPropertyCodUnidOper()
    {
    }

    /**
     * Test attribute "cnpj_unid_oper"
     */
    public function testPropertyCnpjUnidOper()
    {
    }

    /**
     * Test attribute "nom_unid_oper"
     */
    public function testPropertyNomUnidOper()
    {
    }

    /**
     * Test attribute "nom_fantasia"
     */
    public function testPropertyNomFantasia()
    {
    }

    /**
     * Test attribute "nom_contato"
     */
    public function testPropertyNomContato()
    {
    }

    /**
     * Test attribute "email_contato"
     */
    public function testPropertyEmailContato()
    {
    }

    /**
     * Test attribute "tel_contato"
     */
    public function testPropertyTelContato()
    {
    }

    /**
     * Test attribute "ind_util_vertis"
     */
    public function testPropertyIndUtilVertis()
    {
    }

    /**
     * Test attribute "id_nl_laudos_disp"
     */
    public function testPropertyIdNlLaudosDisp()
    {
    }

    /**
     * Test attribute "cod_ord_servico"
     */
    public function testPropertyCodOrdServico()
    {
    }

    /**
     * Test attribute "dth_req_ord_servico"
     */
    public function testPropertyDthReqOrdServico()
    {
    }

    /**
     * Test attribute "dth_publicacao_os"
     */
    public function testPropertyDthPublicacaoOs()
    {
    }

    /**
     * Test attribute "dth_download"
     */
    public function testPropertyDthDownload()
    {
    }

    /**
     * Test attribute "nom_paciente"
     */
    public function testPropertyNomPaciente()
    {
    }

    /**
     * Test attribute "nom_responsavel"
     */
    public function testPropertyNomResponsavel()
    {
    }

    /**
     * Test attribute "ind_com_imagem"
     */
    public function testPropertyIndComImagem()
    {
    }

    /**
     * Test attribute "ind_anexos"
     */
    public function testPropertyIndAnexos()
    {
    }

    /**
     * Test attribute "ind_evolutivo"
     */
    public function testPropertyIndEvolutivo()
    {
    }

    /**
     * Test attribute "id_nl_laudos_exames"
     */
    public function testPropertyIdNlLaudosExames()
    {
    }

    /**
     * Test attribute "fkid_nl_laudos_disp"
     */
    public function testPropertyFkidNlLaudosDisp()
    {
    }

    /**
     * Test attribute "cod_ord_serv_item"
     */
    public function testPropertyCodOrdServItem()
    {
    }

    /**
     * Test attribute "cod_exame"
     */
    public function testPropertyCodExame()
    {
    }

    /**
     * Test attribute "dth_publicacao_ex"
     */
    public function testPropertyDthPublicacaoEx()
    {
    }

    /**
     * Test attribute "nom_exame"
     */
    public function testPropertyNomExame()
    {
    }

    /**
     * Test attribute "id_nl_ldo_exa_dicom"
     */
    public function testPropertyIdNlLdoExaDicom()
    {
    }

    /**
     * Test attribute "link_img_dicom"
     */
    public function testPropertyLinkImgDicom()
    {
    }

    /**
     * Test attribute "id_amostra"
     */
    public function testPropertyIdAmostra()
    {
    }

    /**
     * Test attribute "downloads"
     */
    public function testPropertyDownloads()
    {
    }

    /**
     * Test attribute "dth_prev_entrega"
     */
    public function testPropertyDthPrevEntrega()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
