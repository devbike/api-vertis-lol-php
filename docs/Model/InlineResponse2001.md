# InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**os** | **string** | Operating System Information | [optional] 
**cpu_count** | **int** | Numbers of cores available | [optional] 
**cpu_architecture** | **string** | CPU&#39;s Architecture | [optional] 
**system_time** | **string** | Server timestamp | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


