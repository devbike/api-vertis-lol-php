# ServiceMailAnexos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nome** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 
**arquivo_b64** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


