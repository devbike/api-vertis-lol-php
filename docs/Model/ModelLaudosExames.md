# ModelLaudosExames

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_unid_oper** | **int** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cnpj_unid_oper** | **string** | #field_definition# | 
**nom_unid_oper** | **string** | #field_definition# | 
**nom_fantasia** | **string** | #field_definition# | 
**nom_contato** | **string** | #field_definition# | 
**email_contato** | **string** | #field_definition# | 
**tel_contato** | **string** | #field_definition# | 
**ind_util_vertis** | **string** | #field_definition# | 
**id_nl_laudos_disp** | **int** | #field_definition# | 
**cod_ord_servico** | **int** | #field_definition# | 
**dth_req_ord_servico** | **string** | #field_definition# | 
**dth_publicacao_os** | **string** | #field_definition# | 
**dth_download** | **string** | #field_definition# | 
**nom_paciente** | **string** | #field_definition# | 
**nom_responsavel** | **string** | #field_definition# | 
**ind_com_imagem** | **string** | #field_definition# | 
**ind_anexos** | **string** | #field_definition# | 
**ind_evolutivo** | **string** | #field_definition# | 
**id_nl_laudos_exames** | **int** | #field_definition# | 
**fkid_nl_laudos_disp** | **int** | #field_definition# | 
**cod_ord_serv_item** | **int** | #field_definition# | 
**cod_exame** | **int** | #field_definition# | 
**dth_publicacao_ex** | **string** | #field_definition# | 
**nom_exame** | **string** | #field_definition# | 
**id_nl_ldo_exa_dicom** | **int** | #field_definition# | 
**link_img_dicom** | **string** | #field_definition# | 
**id_amostra** | **string** | #field_definition# | 
**downloads** | [**\VertisLol\Model\ModelLaudosDownloads[]**](ModelLaudosDownloads.md) | arquivos disponíveis para download dos exames que compôem a OS | 
**dth_prev_entrega** | **string** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


