# ModelLaudoParceiros

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_nl_ldo_disp_parceiro** | **int** | #field_definition# | 
**fkid_nl_laudos_disp** | **int** | #field_definition# | 
**fkid_parceiro** | **int** | #field_definition# | 
**ind_laudo_lido** | **string** | #field_definition# | 
**cod_unid_negoc** | **int** |  | [optional] 
**cod_unid_oper** | **int** |  | [optional] 
**cod_ord_servico** | **int** |  | [optional] 
**cod_parceiro** | **int** |  | [optional] 
**contingencia** | **bool** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


