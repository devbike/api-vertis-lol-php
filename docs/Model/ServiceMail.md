# ServiceMail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **string** |  | [optional] 
**port** | **int** |  | [optional] 
**user_name** | **string** |  | [optional] 
**password** | **string** |  | [optional] 
**connect_timeout** | **int** |  | [optional] 
**ssl_connection** | **bool** |  | [optional] 
**tls_mode** | **string** |  | [optional] 
**nome_remetente** | **string** |  | [optional] 
**e_mail_remetente** | **string** |  | [optional] 
**destinatarios** | **string** |  | [optional] 
**assunto** | **string** |  | [optional] 
**message_body** | **string** |  | [optional] 
**unid_oper** | **int** |  | [optional] 
**unid_neg** | **int** |  | [optional] 
**modo** | **string** |  | [optional] 
**path_log** | **string** |  | [optional] 
**nome_log** | **string** |  | [optional] 
**anexos** | [**\VertisLol\Model\ServiceMailAnexos[]**](ServiceMailAnexos.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


