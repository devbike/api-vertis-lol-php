# MVCErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**classname** | **string** |  | [optional] 
**detailedmessage** | **string** |  | [optional] 
**apperrorcode** | **int** |  | [optional] 
**items** | [**\VertisLol\Model\MVCErrorResponseItems[]**](MVCErrorResponseItems.md) |  | [optional] 
**statuscode** | **int** |  | [optional] 
**reasonstring** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**data** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


