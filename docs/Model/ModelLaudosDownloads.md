# ModelLaudosDownloads

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_unid_oper** | **int** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cnpj_unid_oper** | **string** | #field_definition# | 
**nom_unid_oper** | **string** | #field_definition# | 
**nom_fantasia** | **string** | #field_definition# | 
**nom_contato** | **string** | #field_definition# | 
**email_contato** | **string** | #field_definition# | 
**tel_contato** | **string** | #field_definition# | 
**ind_util_vertis** | **string** | #field_definition# | 
**id_nl_laudos_disp** | **int** | #field_definition# | 
**cod_ord_servico** | **int** | #field_definition# | 
**dth_req_ord_servico** | **string** | #field_definition# | 
**dth_publicacao** | **string** | #field_definition# | 
**dth_download** | **string** | #field_definition# | 
**nom_paciente** | **string** | #field_definition# | 
**nom_responsavel** | **string** | #field_definition# | 
**ind_com_imagem** | **string** | #field_definition# | 
**ind_anexos** | **string** | #field_definition# | 
**ind_evolutivo** | **string** | #field_definition# | 
**id_nl_ldo_disp_anexos** | **int** | #field_definition# | 
**fkid_nl_laudos_disp** | **int** | #field_definition# | 
**cod_ord_serv_item** | **int** | #field_definition# | 
**ind_tipo_arquivo** | **string** | #field_definition# | 
**nom_arq_anexado** | **string** | #field_definition# | 
**link_laudo** | **string** | #field_definition# | 
**id_amostra** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


