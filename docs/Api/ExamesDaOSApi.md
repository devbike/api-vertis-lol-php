# VertisLol\ExamesDaOSApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExames**](ExamesDaOSApi.md#getExames) | **GET** /lol-api/V1.1/laudos-parceiro/exames/{id} | 


# **getExames**
> \VertisLol\Model\ModelLaudosExames getExames($id)



Retorna os exames existentes na Ordem de Serviço definida no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\ExamesDaOSApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | id da OS

try {
    $result = $apiInstance->getExames($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesDaOSApi->getExames: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| id da OS |

### Return type

[**\VertisLol\Model\ModelLaudosExames**](../Model/ModelLaudosExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

