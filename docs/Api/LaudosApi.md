# VertisLol\LaudosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLaudos**](LaudosApi.md#getLaudos) | **GET** /lol-api/V1.1/laudos-parceiro/{P} | 
[**sendMail**](LaudosApi.md#sendMail) | **POST** /lol-api/V1.1/sendmail | 


# **getLaudos**
> \VertisLol\Model\ModelLaudos[] getLaudos($p)



Retorna registros do objeto laudos_disponiveis

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\LaudosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$p = "p_example"; // string | Periodo da Ordem de Serviço

try {
    $result = $apiInstance->getLaudos($p);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LaudosApi->getLaudos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **string**| Periodo da Ordem de Serviço |

### Return type

[**\VertisLol\Model\ModelLaudos[]**](../Model/ModelLaudos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendMail**
> \VertisLol\Model\ServiceMail[] sendMail()



Retorna registros do objeto laudos_disponiveis

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\LaudosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->sendMail();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LaudosApi->sendMail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisLol\Model\ServiceMail[]**](../Model/ServiceMail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

