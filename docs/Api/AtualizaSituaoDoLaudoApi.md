# VertisLol\AtualizaSituaoDoLaudoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**setDownloadedItem**](AtualizaSituaoDoLaudoApi.md#setDownloadedItem) | **GET** /lol-api/V1.1/laudos-parceiro/downloads/{id}/lido | 


# **setDownloadedItem**
> \VertisLol\Model\ModelLaudoParceiros setDownloadedItem($laudos_lido, $id)



Retorna um laudo específico, definido no parâmetrp IT, disponível para o download na Ordem de Serviço definida no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\AtualizaSituaoDoLaudoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$laudos_lido = new \VertisLol\Model\ModelLaudoParceiros(); // \VertisLol\Model\ModelLaudoParceiros | Objeto
$id = 56; // int | ID do Laudo

try {
    $result = $apiInstance->setDownloadedItem($laudos_lido, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AtualizaSituaoDoLaudoApi->setDownloadedItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **laudos_lido** | [**\VertisLol\Model\ModelLaudoParceiros**](../Model/ModelLaudoParceiros.md)| Objeto |
 **id** | **int**| ID do Laudo |

### Return type

[**\VertisLol\Model\ModelLaudoParceiros**](../Model/ModelLaudoParceiros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

