# VertisLol\JWTAuthenticationApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginPost**](JWTAuthenticationApi.md#loginPost) | **POST** /login | 


# **loginPost**
> \VertisLol\Model\InlineResponse200 loginPost($jwtusername, $jwtpassword)



Create JSON Web Token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basic
$config = VertisLol\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new VertisLol\Api\JWTAuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$jwtusername = "jwtusername_example"; // string | 
$jwtpassword = "jwtpassword_example"; // string | 

try {
    $result = $apiInstance->loginPost($jwtusername, $jwtpassword);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JWTAuthenticationApi->loginPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jwtusername** | **string**|  | [optional]
 **jwtpassword** | **string**|  | [optional]

### Return type

[**\VertisLol\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[basic](../../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

