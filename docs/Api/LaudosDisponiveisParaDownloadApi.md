# VertisLol\LaudosDisponiveisParaDownloadApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDownloadItem**](LaudosDisponiveisParaDownloadApi.md#getDownloadItem) | **GET** /lol-api/V1.1/laudos-parceiro/downloads/{id}/item/{it} | 
[**getDownloads**](LaudosDisponiveisParaDownloadApi.md#getDownloads) | **GET** /lol-api/V1.1/laudos-parceiro/downloads/{id} | 


# **getDownloadItem**
> \VertisLol\Model\ModelLaudosDownload getDownloadItem($it, $id)



Retorna um laudo específico, definido no parâmetrp IT, disponível para o download na Ordem de Serviço definida no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\LaudosDisponiveisParaDownloadApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$it = 56; // int | 
$id = 56; // int | ID da Ordem de Serviço

try {
    $result = $apiInstance->getDownloadItem($it, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LaudosDisponiveisParaDownloadApi->getDownloadItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **it** | **int**|  |
 **id** | **int**| ID da Ordem de Serviço |

### Return type

[**\VertisLol\Model\ModelLaudosDownload**](../Model/ModelLaudosDownload.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDownloads**
> \VertisLol\Model\ModelLaudosDownload getDownloads($id)



Retorna os laudos disponíveis para o download na Ordem de Serviço definida no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\LaudosDisponiveisParaDownloadApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID da Ordem de Serviço

try {
    $result = $apiInstance->getDownloads($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LaudosDisponiveisParaDownloadApi->getDownloads: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID da Ordem de Serviço |

### Return type

[**\VertisLol\Model\ModelLaudosDownload**](../Model/ModelLaudosDownload.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

