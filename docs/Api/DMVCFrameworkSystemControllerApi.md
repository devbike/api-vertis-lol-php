# VertisLol\DMVCFrameworkSystemControllerApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform**](DMVCFrameworkSystemControllerApi.md#mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform) | **GET** /system/describeplatform.info | 
[**mVCFrameworkSysControllersTMVCSystemControllerDescribeServer**](DMVCFrameworkSystemControllerApi.md#mVCFrameworkSysControllersTMVCSystemControllerDescribeServer) | **GET** /system/describeserver.info | 
[**mVCFrameworkSysControllersTMVCSystemControllerServerConfig**](DMVCFrameworkSystemControllerApi.md#mVCFrameworkSysControllersTMVCSystemControllerServerConfig) | **GET** /system/serverconfig.info | 


# **mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform**
> \VertisLol\Model\InlineResponse2001 mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform()



Describe the system where server is running

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\DMVCFrameworkSystemControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DMVCFrameworkSystemControllerApi->mVCFrameworkSysControllersTMVCSystemControllerDescribePlatform: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisLol\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mVCFrameworkSysControllersTMVCSystemControllerDescribeServer**
> mVCFrameworkSysControllersTMVCSystemControllerDescribeServer()



Describe controllers and actions published by the RESTful server per resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\DMVCFrameworkSystemControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->mVCFrameworkSysControllersTMVCSystemControllerDescribeServer();
} catch (Exception $e) {
    echo 'Exception when calling DMVCFrameworkSystemControllerApi->mVCFrameworkSysControllersTMVCSystemControllerDescribeServer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mVCFrameworkSysControllersTMVCSystemControllerServerConfig**
> mVCFrameworkSysControllersTMVCSystemControllerServerConfig()



Server configuration

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisLol\Api\DMVCFrameworkSystemControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->mVCFrameworkSysControllersTMVCSystemControllerServerConfig();
} catch (Exception $e) {
    echo 'Exception when calling DMVCFrameworkSystemControllerApi->mVCFrameworkSysControllersTMVCSystemControllerServerConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

